import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';
  constructor(private router:Router,private translate: TranslateService){
    translate.setDefaultLang('en');
  }
  goAbout(){
    this.router.navigate(['/about']);
  }
  goContact(){
    this.router.navigate(['/contact']);
  }
  goCoordonator(){
    this.router.navigate(['/coordonator']);
  }
  goHome() {
    this.router.navigate(['/home']);
  }
  goNoutati(){
  this.router.navigate(['/noutati']);
  }
  goProfil(){
    this.router.navigate(['/profil']);
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
}

