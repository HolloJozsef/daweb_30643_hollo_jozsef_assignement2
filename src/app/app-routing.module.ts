import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NoutatiComponent} from "./noutati/noutati.component";
import {ContactComponent} from "./contact/contact.component";
import {HomeComponent} from "./home/home.component";
import {ProfilComponent} from "./profil/profil.component";
import {AboutComponent} from "./about/about.component";
import {CoordonatorComponent} from "./coordonator/coordonator.component";

const routes: Routes=[{path:'noutati',component :NoutatiComponent},
  {path:'contact',component :ContactComponent},
  {path:'home',component :HomeComponent},
  {path:'profil',component :ProfilComponent},
  {path:'about',component :AboutComponent},
  {path:'coordonator',component :CoordonatorComponent}
  ];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
